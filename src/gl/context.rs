use crate::game::graphics::context::GraphicsContext;
use crate::game::graphics::Color;
use crate::game::error::GameResult;
use crate::gl::sprite_batch::SpriteBatch;
use glium::{Frame, Surface};

pub struct GlGraphicsContext {
    target: Frame,
}

impl GlGraphicsContext {
    pub(crate) fn new(target: Frame) -> GlGraphicsContext {
        GlGraphicsContext {
            target
        }
    }

    pub(crate) fn finish(self) -> Frame {
        self.target
    }
}

impl GraphicsContext for GlGraphicsContext {
    type SpriteBatch = SpriteBatch;

    fn new_sprite_batch(&mut self, capacity: Option<usize>) -> GameResult<Self::SpriteBatch> {
        Self::SpriteBatch::new()
    }

    fn clear_color(&mut self, color: Color) -> GameResult {
        self.target.clear_color(color.red, color.green, color.blue, color.alpha);
        Ok(())
    }
}