use crate::game::graphics::{sprite_batch, SpriteSortMode};
use crate::game::graphics::sprite_batch::{BeginOptions, SpriteBatchError};
use crate::game::error::GameResult;

pub struct SpriteBatch {
    begin_called: bool,
    sort_mode: SpriteSortMode,
}

impl SpriteBatch {
    pub fn new() -> GameResult<SpriteBatch> {
        Ok(SpriteBatch {
            begin_called: false,
            sort_mode: SpriteSortMode::Deferred
        })
    }
}

impl sprite_batch::SpriteBatch for SpriteBatch {
    fn begin(&mut self, options: BeginOptions) -> GameResult<()> {
        if self.begin_called {
            return Err(SpriteBatchError::BeginCalledAgainWithoutEnd.into())
        }

        self.sort_mode = options.sprite_sort_mode;

        self.begin_called = true;

        Ok(())
    }

    fn end(&mut self) -> GameResult<()> {
        if !self.begin_called {
            return Err(SpriteBatchError::EndCalledWithoutBegin.into())
        }

        self.begin_called = false;

        // Actually draw stuff

        Ok(())
    }
}