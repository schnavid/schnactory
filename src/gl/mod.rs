pub mod sprite_batch;
pub mod context;

use crate::{
    game::{
        error::GameResult,
        graphics::device::GraphicsDevice,
        Game
    }
};
use glium::{
    Display,
    Surface,
    glutin::{
        event_loop::{
            EventLoop,
            ControlFlow
        },
        window::WindowBuilder,
        ContextBuilder,
        event::{Event, WindowEvent},
    }
};
use crate::gl::context::GlGraphicsContext;

pub struct GlGraphicsDevice {
    event_loop: Option<EventLoop<()>>,
    display: Option<Display>,
}

impl GraphicsDevice for GlGraphicsDevice {
    fn new() -> GameResult<Self> {
        let event_loop = EventLoop::new();
        let wb = WindowBuilder::new().with_title("Game Window");
        let cb = ContextBuilder::new();
        let display = Display::new(wb, cb, &event_loop)?;

        Ok(GlGraphicsDevice {
            event_loop: Some(event_loop),
            display: Some(display)
        })
    }

    fn run<G: 'static + Game>(&mut self, game: G) -> GameResult<()> {
        let display = self.display.take().unwrap();
        let event_loop = self.event_loop.take().unwrap();
        let mut game = game;

        event_loop.run(move |ev, _, control_flow| {
            let mut target = display.draw();
            let mut ctx = GlGraphicsContext::new(target);
            game.draw(&mut ctx).unwrap();
            ctx.finish().finish().unwrap();

            let next_frame_time = std::time::Instant::now() + std::time::Duration::from_nanos(16_666_667);

            *control_flow = ControlFlow::WaitUntil(next_frame_time);
            match ev {
                Event::WindowEvent { event, .. } => match event {
                    WindowEvent::CloseRequested => {
                        *control_flow = ControlFlow::Exit;
                        return;
                    },
                    _ => return,
                },
                _ => (),
            }
        });
    }
}