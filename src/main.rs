#[macro_use]
extern crate glium;

pub mod game;
pub mod gl;
pub mod schnactory;

use crate::gl::GlGraphicsDevice;
use crate::game::graphics::device::GraphicsDevice;
use crate::game::error::GameResult;
use crate::schnactory::Schnactory;
use crate::game::Game;

fn main() -> GameResult {
    GlGraphicsDevice::new()?.run(Schnactory::new(60.0)?)
}