use crate::game::Game;
use crate::game::error::GameResult;
use crate::game::graphics::context::GraphicsContext;
use crate::game::content::ContentRegistry;
use crate::game::graphics::Color;

pub struct Schnactory {

}

impl Game for Schnactory {
    fn new(_fps: f32) -> GameResult<Self> {
        Ok(Schnactory {})
    }

    fn load<C: ContentRegistry>(&mut self, _reg: &mut C) -> GameResult {
        Ok(())
    }

    fn update(&mut self) -> GameResult<()> {
        Ok(())
    }

    fn draw<G: GraphicsContext>(&mut self, gfx: &mut G) -> GameResult {
        gfx.clear_color(Color::CORNFLOWER_BLUE)?;

        Ok(())
    }
}