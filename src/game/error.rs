use std::error::Error;
use std::fmt::{Display, Formatter};
use glium::backend::glutin::DisplayCreationError;
use crate::game::graphics::sprite_batch::SpriteBatchError;

pub type GameResult<T = ()> = Result<T, GameError>;

#[derive(Debug)]
pub enum GameError {
    DisplayCreationError(DisplayCreationError),
    SpriteBatchError(SpriteBatchError),
}

impl From<DisplayCreationError> for GameError {
    fn from(e: DisplayCreationError) -> Self {
        GameError::DisplayCreationError(e)
    }
}

impl From<SpriteBatchError> for GameError {
    fn from(e: SpriteBatchError) -> Self {
        GameError::SpriteBatchError(e)
    }
}

impl Display for GameError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "an error has occured")
    }
}

impl Error for GameError {}