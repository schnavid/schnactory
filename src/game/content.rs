use crate::game::error::GameResult;
use std::path::Path;

pub trait ContentRegistry {
    fn load_texture<S: Into<String>>(name: S, path: Path) -> GameResult;
}