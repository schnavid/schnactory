use crate::{
    game::{
        error::GameResult,
        graphics::context::GraphicsContext
    }
};
use crate::game::content::ContentRegistry;

pub mod error;
pub mod graphics;
pub mod content;

pub trait Game: Sized {
    fn new(fps: f32) -> GameResult<Self>;

    fn load<C: ContentRegistry>(&mut self, reg: &mut C) -> GameResult;
    fn update(&mut self) -> GameResult;
    fn draw<G: GraphicsContext>(&mut self, gfx: &mut G) -> GameResult;
}