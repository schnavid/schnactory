use crate::{
    game::{
        graphics::{
            sprite_batch::SpriteBatch,
            Color
        },
        error::GameResult,
    }
};

pub trait GraphicsContext: Sized {
    type SpriteBatch: SpriteBatch;

    fn new_sprite_batch(&mut self, capacity: Option<usize>) -> GameResult<Self::SpriteBatch>;
    fn clear_color(&mut self, color: Color) -> GameResult;
}