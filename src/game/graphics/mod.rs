use crate::game::error::GameResult;
use crate::game::Game;

pub mod sprite_batch;
pub mod device;
pub mod context;

pub enum SpriteSortMode {
    Deferred,
    Immediate,
    Texture,
    BackToFront,
    FrontToBack,
}

pub struct Color {
    pub red: f32,
    pub green: f32,
    pub blue: f32,
    pub alpha: f32,
}

impl Color {
    pub const RED: Color = Color {
        red: 1.0,
        green: 0.0,
        blue: 0.0,
        alpha: 1.0
    };

    pub const CORNFLOWER_BLUE: Color = Color {
        red: 100.0 / 255.0,
        green: 149.0 / 255.0,
        blue: 237.0 / 255.0,
        alpha: 1.0
    };
}