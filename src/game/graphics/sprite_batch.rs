use crate::game::error::GameResult;
use crate::game::graphics::SpriteSortMode;

type BlendState = ();
type SamplerState = ();
type DepthStencilState = ();
type RasterizerState = ();
type Effect = ();
type Matrix = ();

pub struct BeginOptions {
    pub(crate) sprite_sort_mode: SpriteSortMode,
    blend_state: Option<BlendState>,
    sampler_state: Option<SamplerState>,
    depth_stencil_state: Option<DepthStencilState>,
    rasterizer_state: Option<RasterizerState>,
    effect: Option<Effect>,
    transform_matrix: Option<Matrix>,
}

impl BeginOptions {
    pub fn new() -> BeginOptions { BeginOptions::default() }
    pub fn sprite_sort_mode(mut self, sprite_sort_mode: SpriteSortMode) -> BeginOptions {
        self.sprite_sort_mode = sprite_sort_mode;
        self
    }
}

impl Default for BeginOptions {
    fn default() -> Self {
        BeginOptions {
            sprite_sort_mode: SpriteSortMode::Deferred,
            blend_state: None,
            sampler_state: None,
            depth_stencil_state: None,
            rasterizer_state: None,
            effect: None,
            transform_matrix: None
        }
    }
}

#[derive(Debug)]
pub enum SpriteBatchError {
    BeginCalledAgainWithoutEnd,
    EndCalledWithoutBegin,
}

pub trait SpriteBatch: Sized {
    fn begin(&mut self, options: BeginOptions) -> GameResult;
    fn end(&mut self) -> GameResult;
}