use crate::game::Game;
use crate::game::error::GameResult;

pub trait GraphicsDevice: Sized {
    fn new() -> GameResult<Self>;
    fn run<G: 'static + Game>(&mut self, game: G) -> GameResult;
}